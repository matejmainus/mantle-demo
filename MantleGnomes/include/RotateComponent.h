#pragma once

#include <GRSceneObjectComponent.h>
#include <SceneObjectComponent/GRTransform.h>

namespace MantleGnomes
{

	class RotateComponent : public GREngine::SceneObjectComponent
	{
	public:
		RotateComponent(GREngine::SceneObject *obj, float rotateSpeed);
		~RotateComponent();

		virtual void update(unsigned int timeInMilis) override;

	private:
		float mRotateSpeed;

		GREngine::SceneObjectComponents::TransformSPtr mTransform;
	};

}