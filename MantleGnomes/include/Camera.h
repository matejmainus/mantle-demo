#pragma once

#include <GRSceneObjectComponent.h>
#include <SceneObjectComponent/GRTransform.h>

namespace MantleGnomes
{

class Camera : public GREngine::SceneObjectComponent
{
public:
	Camera(GREngine::SceneObject *sceneObject);
	~Camera();

	virtual void update(unsigned int timeInMilis) override;

private:
	float moveSpeed = 0.02f;
	float rotationSpeed = 0.01f;

	int mMouseX = 0;
	int mMouseY = 0;
		
	GREngine::SceneObjectComponents::TransformSPtr mTransform;
};

}