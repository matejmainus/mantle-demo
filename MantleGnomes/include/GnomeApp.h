#pragma once

#include "GRApp.h"

namespace MantleGnomes
{

	class GnomeApp : public GREngine::App
	{
	public:
		GnomeApp();
		virtual ~GnomeApp();

		virtual void init() override;
		virtual void shutdown() override;

		virtual void start() override;
		virtual void stop() override;

		virtual void update(int timeInMilis) override;

	private:
		void prepareBunnyScene(const GREngine::SceneSPtr &scene, const GREngine::WindowSPtr &renderTarget);

	private:
		GREngine::WindowSPtr mMonkeyWindow;
		GREngine::WindowSPtr mBunnyWindow;
	};

}