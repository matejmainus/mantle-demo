#include <iostream>

#include <random>

#include <GRKernel.h>
#include <GRScene.h>
#include <GRSceneObject.h>
#include <GRSceneObjectComponent.h>
#include <SceneObjectComponent/GRCamera.h>
#include <SceneObjectComponent/GRMeshRenderer.h>
#include <SceneObjectComponent/GRTransform.h>
#include <SceneObjectComponent/GRLight.h>
#include <Material/GRSimpleMaterial.h>
#include <GRMesh.h>
#include <GRSceneManager.h>
#include <GRTexture.h>
#include <GRWindow.h>

#include "Camera.h"
#include "RotateComponent.h"
#include "ChangeMaterialComponent.h"
#include "GnomeApp.h"

namespace MantleGnomes
{

	using namespace GREngine;
	using namespace GREngine::SceneObjectComponents;
	using namespace GREngine::Materials;

	const int WINDOW_WIDTH = 800;
	const int WINDOW_HEIGHT = 600;

	GnomeApp::GnomeApp() :
		GREngine::App()
	{
	}

	GnomeApp::~GnomeApp()
	{
	}

	void GnomeApp::init()
	{
		//mMonkeyWindow = std::make_shared<Window>("Monkeys", WINDOW_WIDTH, WINDOW_HEIGHT);
		mBunnyWindow = std::make_shared<Window>("Bunnys", WINDOW_WIDTH, WINDOW_HEIGHT);

		Kernel *kernel = Kernel::getInstancePtr();
		//kernel->addEventHandler(mMonkeyWindow);
		kernel->addEventHandler(mBunnyWindow);

		SceneManager *sceneManager = SceneManager::getInstancePtr();

		//SceneSPtr monkeyScene = sceneManager->createScene("Monkeys").lock();
		//prepareBunnyScene(monkeyScene, mMonkeyWindow);

		SceneSPtr bunnyScene = sceneManager->createScene("Bunnys").lock();
		prepareBunnyScene(bunnyScene, mBunnyWindow);
	}

	void GnomeApp::shutdown()
	{
		mMonkeyWindow.reset();
		mBunnyWindow.reset();
	}

	void GnomeApp::start()
	{
	}

	void GnomeApp::stop()
	{
	}

	void GnomeApp::prepareBunnyScene(const SceneSPtr &scene, const WindowSPtr &renderTarget)
	{
		SceneObjectSPtr sceneRoot = scene->rootObject();

		SceneObjectSPtr camera = sceneRoot->createChild("MainCamera");
		camera->createComponent<MantleGnomes::Camera>();

		TransformSPtr cameraTransform = camera->component<Transform>();
		cameraTransform->moveTo({ 0, 0, -20 });

		CameraSPtr camComponent = camera->createComponent<GREngine::SceneObjectComponents::Camera>();
		camComponent->setRenderTarget(renderTarget);

		LightSPtr lightComponent = camera->createComponent<GREngine::SceneObjectComponents::Light>();
		lightComponent->setIntensity(1.4f);
		lightComponent->setColor({0.8f, 1.0f, 0.8f});

		scene->activateViewport(camComponent);

		MeshSPtr bunnyMesh = Mesh::create("mesh/bunny.obj");
		MeshSPtr monkeyMesh = Mesh::create("mesh/monkey.obj");
		TextureSPtr plasticTexture = Texture::createTexture("textures/plastic.jpg");
		TextureSPtr paperTexture = Texture::createTexture("textures/paper.jpg");

		std::random_device rd;
		std::mt19937 gen(rd());

		std::bernoulli_distribution typeDistr;
		std::uniform_real_distribution<float> colorDistr(0.0f, 1.f);
		std::uniform_int_distribution<int> switchTimeDist(500, 2000);
		std::uniform_real_distribution<float> rotSpeedDist(-0.6f, 0.6f);

		const int MAX = (int) sqrt(1300);

		float xOffset = 10.0f;
		float yOffset = 10.0f;

		bool monkey = false;
		bool paper = false;

		for (int y = 0; y < MAX; ++y)
		{
			for (int x = 0; x < MAX; ++x)
			{
				monkey = typeDistr(gen);
				paper = typeDistr(gen);

				SceneObjectSPtr animal = sceneRoot->createChild("Animal");

				TransformSPtr animalTransform = animal->component<Transform>();
				animalTransform->moveTo({
					(x - MAX / 2) * xOffset,
					(y - MAX / 2) * yOffset,
					0.0f });

				animalTransform->scale({ 1, 1, 1 });

				MeshRendererSPtr renderer = animal->createComponent<MeshRenderer>();
				SimpleMaterialSPtr material = SimpleMaterial::create({
					colorDistr(gen), colorDistr(gen), colorDistr(gen)},
					paper ? paperTexture : plasticTexture
				);

				animal->createComponent<ChangeMaterialComponent>(material, switchTimeDist(gen));
				animal->createComponent<RotateComponent>(rotSpeedDist(gen));
				
				renderer->setMaterial(material);
				//renderer->setMesh(monkeyMesh);
				renderer->setMesh(monkey ? monkeyMesh : bunnyMesh);
			}
		}
	}

	void GnomeApp::update(int timeInMilis)
	{
	}

}