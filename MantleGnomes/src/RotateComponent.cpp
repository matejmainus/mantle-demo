#include "..\include\RotateComponent.h"

#include <random>
#include <GRSceneObject.h>


namespace MantleGnomes
{
	using namespace GREngine;
	using namespace GREngine::SceneObjectComponents;

	RotateComponent::RotateComponent(SceneObject *obj, float rotateSpeed) :
		SceneObjectComponent(obj),
		mTransform(obj->component<Transform>()),
		mRotateSpeed(rotateSpeed)
	{
	}

	RotateComponent::~RotateComponent()
	{
	}

	void RotateComponent::update(unsigned int timeInMilis)
	{
		mTransform->rotateByAngles({ 0.0f, timeInMilis * mRotateSpeed, 0.0f });
	}

}