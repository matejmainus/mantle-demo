# Mantle Demo #

This project is just a tech demo of Mantle API. 
Does not contain lot of common techniques used in other engines. 

It shows simple usage of Mantle API and design of multi thread renderer.
Feel free to use it into your engine based on next-get API like Mantle or Vulkan

# Structure #
Project is divided into 2 parts. Code for something like game engine is in library called GREngine. 

App specific code in GnomeApp module.

Does not contain scripts to copy app resources into working directory.
Does not contain libraries (SDL, Assimp, FreeImage, Magma)