#pragma once

#include "GRPrereq.h"

#include <vector>

namespace GREngine
{
	class SceneObject
	{
	public:
		explicit SceneObject(const std::string &name, Scene *scene, SceneObject *parent);
		virtual ~SceneObject();

		const std::string& name() const;

		template<typename T>
		std::shared_ptr<T> createComponent();

		template<typename T, typename... Args> 
		std::shared_ptr<T> createComponent(Args &&... args);

		template<typename ... Args>
		SceneObjectComponents::TransformSPtr createComponent(Args &&... args);

		template<typename T>
		void removeComponent(const std::shared_ptr<T>& component);
		
		template<typename T>
		std::shared_ptr<T> component() const;

		template<>
		inline std::shared_ptr<SceneObjectComponents::Transform> component() const
		{
			return mTransform;
		}

		void components(std::vector<SceneObjectComponentSPtr> &components);

		template<typename T>
		void components(std::vector<std::shared_ptr<T>> &components);

		SceneObjectSPtr createChild(const std::string &name);
		void removeChild(const SceneObjectSPtr &child);
		const SceneObjectSPtr& child(size_t index) const;

		void childs(std::vector<SceneObjectSPtr> &childs) const;

		void update(unsigned int timeInMilis);

		bool active() const;
		void setActive(bool active);

	private:
		Scene *mScene;
		SceneObject *mParent;

		std::atomic<bool> mActive;

	private:
		template<typename T>
		void addComponent(const std::shared_ptr<T>& component);

	private:
		mutable std::mutex mComponentsMutex;
		mutable std::mutex mChildsMutex;

		std::string mName;

		SceneObjectComponents::TransformSPtr mTransform;

		std::vector<SceneObjectComponentSPtr> mComponents;
		std::vector<SceneObjectSPtr> mChilds;
	};

	template<typename T>
	inline std::shared_ptr<T> SceneObject::createComponent()
	{
		std::shared_ptr<T> ptr = std::make_shared<T>(this);

		addComponent(ptr);

		return ptr;
	}

	template<typename T, typename... Args>
	inline std::shared_ptr<T> SceneObject::createComponent(Args &&... args)
	{
		std::shared_ptr<T> ptr = std::make_shared<T>(this, args...);

		addComponent(ptr);

		return ptr;
	}

	template<typename T>
	inline std::shared_ptr<T> SceneObject::component() const
	{
		MutexGuard locker(mComponentsMutex);

		std::shared_ptr<T> ptr;
		for (const SceneObjectComponentSPtr &soc : mComponents)
		{
			ptr = std::dynamic_pointer_cast<T>(soc);
			if (ptr)
				return ptr;
		}

		return nullptr;
	}

	template<typename T>
	inline void SceneObject::addComponent(const std::shared_ptr<T>& component)
	{
		MutexGuard locker(mComponentsMutex);

		mComponents.push_back(component);
	}

	template<typename T>
	inline void SceneObject::removeComponent(const std::shared_ptr<T> &component)
	{
		MutexGuard locker(mComponentsMutex);

		std::remove_if(mComponents.begin(), mComponents.end(), [component](const SceneObjectComponentSPtr &item) {return component == item; });
	}

	template<typename T>
	inline void SceneObject::components(std::vector<std::shared_ptr<T>>& components)
	{
		MutexGuard locker(mComponentsMutex);

		std::shared_ptr<T> ptr;
		for (const SceneObjectComponentSPtr &soc : mComponents)
		{
			ptr = std::dynamic_pointer_cast<T>(soc);
			if (ptr)
				components.push_back(ptr);
		}

	}


}

