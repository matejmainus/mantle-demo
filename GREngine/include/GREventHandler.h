#pragma once

union SDL_Event;

namespace GREngine
{

	class EventHandler
	{
	public:
		virtual bool handleEvent(const SDL_Event *evt) = 0;
	};
}