#pragma once

#include <vector>

#include "GRPrereq.h"

#include <DirectXMath.h>
#include <DirectXPackedVector.h>

namespace GREngine
{
	struct RenderContext
	{
		RendererSPtr renderer;
		DirectX::XMMATRIX parentTransform;
	};

	struct RenderNode
	{
		PipelineSPtr pipeline;

		std::vector<RenderContext> childs;

		inline static unsigned int maxBatchCount() { return 40; }
	};

	typedef std::shared_ptr<RenderNode> RenderNodeSPtr;

	typedef std::vector<RenderNodeSPtr>::iterator RenderNodeIterator;
	typedef std::vector<RenderNodeSPtr>::const_iterator RenderNodeIterator_const;

	class RenderTree
	{
	public:
		explicit RenderTree();
		~RenderTree();

		void add(const RenderContext context);

		RenderNodeIterator begin();
		RenderNodeIterator end();
		RenderNodeIterator_const begin() const;
		RenderNodeIterator_const end() const;
		RenderNodeIterator_const cbegin() const;
		RenderNodeIterator_const cend() const;

	private:
		
		RenderNodeIterator mStart;

		std::vector<RenderNodeSPtr> mNodes;
	};
}