#pragma once

namespace GREngine
{

	template<typename T>
	class Singleton
	{
	public:
		Singleton()
		{
			sInstance = static_cast<T*>(this);
		}

		virtual ~Singleton()
		{}

		static inline T& getInstance()
		{
			return (*sInstance);
		}

		static inline T* getInstancePtr()
		{
			return sInstance;
		}

	protected:
		static T* sInstance;

	private:
		Singleton(const Singleton<T> &);
		Singleton& operator=(const Singleton<T> &);
	};

	template<typename T>
	T* Singleton<T>::sInstance = 0;
}