#pragma once

#include <SDL.h>

#include "GRPrereq.h"
#include "GRSingleton.h"
#include "GREventHandler.h"

namespace GREngine
{

class InputManager : public Singleton<InputManager>
{
public:
	explicit InputManager();
	~InputManager();

	bool keyState(SDL_Scancode keyCode) const;
	bool mouseButtonState(unsigned char buttonCode) const;

	void mousePosition(int *x, int *y) const;
	void mouseRelativePosition(int *x, int *y) const;
};

}