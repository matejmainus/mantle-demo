#pragma once

#include <atomic>

#include "GREngine.h"

namespace GREngine
{

	class SceneObjectComponent
	{
	public:
		SceneObjectComponent(SceneObject *sceneObj);
		virtual ~SceneObjectComponent();

		virtual void update(unsigned int timeInMilis);

		SceneObject* sceneObject();

		bool active() const;
		void setActive(bool active);

	protected:
		SceneObject *mSceneObject;

		std::atomic<bool> mActive;
	};

}