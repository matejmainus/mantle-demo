#pragma once

#include "GRPrereq.h"

#include "GRMaterial.h"

#include <DirectXMath.h>
#include <DirectXPackedVector.h>

namespace GREngine
{
	namespace Materials
	{

		struct MaterialConstants
		{
			DirectX::XMFLOAT4 color;
		};

		class SimpleMaterial : public Material
		{
		public:
			static SimpleMaterialSPtr create(DirectX::XMVECTOR color, TextureSPtr albedoTexture);

		public:
			SimpleMaterial(DirectX::XMVECTOR color, TextureSPtr albedoTexture);
			virtual ~SimpleMaterial();

			void setColor(DirectX::XMVECTOR color);

			virtual void updateGPUMemory(CommandBufferSPtr &cmdBuff, const RenderPhaseParamsSPtr &renderParams, const RenderContext &context) override;
			virtual GR_MEMORY_VIEW_ATTACH_INFO materialConstantsView(MemoryRefSet &refs) const override;

			virtual void bindData(MemoryRefSet &memoryRefs, const DescriptorSetSPtr &descriptorSet, GR_UINT descriptorOffset,
			const RenderPhaseParamsSPtr &renderParams, const RenderContext &context) override;

		private:
			void initGPUMemory();
			void createPipeline();
			void createSampler();

			static const GR_GRAPHICS_PIPELINE_CREATE_INFO& createPipelineInfo();

		private:
			BufferSPtr mConstantsBuffer;

			MaterialConstants mMaterialConstants;

			TextureSPtr mAlbedoTexture;

			GR_SAMPLER mSampler;
		};

	}

}