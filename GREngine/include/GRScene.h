#pragma once

#include <thread>
#include <vector>
#include <mantle.h>
#include <DirectXMath.h>
#include <DirectXPackedVector.h>

#include "GRPrereq.h"
#include "GRAttachment.h"

namespace GREngine
{
	struct UpdateContext;

	struct RenderSceneConstants
	{
		unsigned int lightsCount;
		DirectX::XMFLOAT3 padding;
	};

	class GRExport Scene 
	{
	public:
		Scene(const std::string &id);
		virtual ~Scene();

		const std::string getId() const;

		bool active() const;
		void setActive(bool active);

		void activateViewport(const ViewportSPtr &viewport);
		void deactivateViewport(const ViewportSPtr &viewport);

		GR_MEMORY_VIEW_ATTACH_INFO constantsView(MemoryRefSet& refs) const;
		GR_MEMORY_VIEW_ATTACH_INFO lightsView(MemoryRefSet& refs) const;

		const SceneObjectSPtr& rootObject() const;

	private:
		bool isRunning() const;
		void stop();
		void start();

		void run();

		void update(unsigned int timeInMilis);

		void initGPUMemory();
		void updateGPUMemory(UpdateContext &updateContext);
		
		void update(UpdateContext &context, const SceneObjectSPtr &sceneObject, unsigned int timeInMilis);
		void draw();

	private:
		mutable std::mutex mRunMutex;
		mutable std::mutex mViewportsMutex;

		std::string mId;
		
		std::atomic<bool> mActive;
		std::thread *mWorkerThread;

		ViewportSPtr mViewport;
		SceneObjectSPtr mRootSceneObject;
		RenderSceneConstants mConstants;

		GPUMemorySPtr mGPUMemory;

		BufferSPtr mConstantsBuffer;
		BufferSPtr mLightsBuffer;
	};
}
