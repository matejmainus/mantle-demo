#pragma once

#include <mutex>
#include <atomic>
#include <iostream>

#include "GREngine.h"

#define GRExport 

namespace GREngine
{
	typedef std::lock_guard<std::mutex> MutexGuard;
	typedef std::unique_lock<std::mutex> MutexUniqueLock;
}
