#pragma once

#include "mantle.h"

#include "GRPrereq.h"
#include "GRGPUMemory.h"

namespace GREngine
{

class Buffer
{
public:
	explicit Buffer(const GPUMemorySPtr &memory, GPUMemory::ObjectAllocInfo &allocInfo);
	explicit Buffer(const GPUMemorySPtr &memory, const GR_GPU_SIZE range, const GR_GPU_SIZE alignment = 0);
	~Buffer();

	GR_GPU_SIZE range() const;
	GR_GPU_SIZE offset() const;
	const GPUMemorySPtr& gpuMemory() const;

	GR_MEMORY_VIEW_ATTACH_INFO grView(const GR_FORMAT format, const GR_GPU_SIZE stride, const GR_GPU_SIZE offset = 0, const GR_GPU_SIZE range = 0) const;
	GR_MEMORY_REF grReference(const GR_ENUM flags = 0) const;

	void update(size_t dataSize, void *data, const GR_GPU_SIZE offset = 0);
	void update(CommandBufferSPtr &cmdBuff, size_t dataSize, void *data, const GR_GPU_SIZE offset = 0);

	bool setState(const GR_ENUM newState, bool blocking = false);
	bool setState(CommandBufferSPtr &cmdBuff, const GR_ENUM newState);

private:
	std::mutex mGuard;

	GPUMemorySPtr mMemory;
	
	GR_MEMORY_VIEW_ATTACH_INFO mInfo;
};

}