#pragma once

#include "GRPrereq.h"

namespace GREngine
{

	class App
	{
	public:
		App();
		virtual ~App();

		virtual void init() = 0;
		virtual void shutdown() = 0;

		virtual void start() = 0;
		virtual void stop() = 0;

		virtual void update(int timeInMilis) {}
	};

}
