#pragma once

#include <mantle.h>
#include <mantleWsiWinExt.h>

#include "GRPrereq.h"
#include "GRRenderTarget.h"
#include "GREventHandler.h"

struct SDL_Window;
struct SDL_WindowEvent;
union SDL_Event;

namespace GREngine
{
	class GRExport Window : public RenderTarget, public EventHandler
	{
	public:
		Window(const std::string &name, unsigned int width, unsigned int height);
		virtual ~Window();

		virtual bool handleEvent(const SDL_Event *event) override;

		virtual void present() override;

	private:

		bool handleWindowEvent(const SDL_WindowEvent *event);

	private:
		SDL_Window *mWindow;

		GR_WSI_WIN_PRESENT_INFO mPresentInfo; 
	};
}