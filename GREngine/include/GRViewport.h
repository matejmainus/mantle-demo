#pragma once

#include <mantle.h>

#include <DirectXMath.h>
#include <DirectXPackedVector.h>

#include "GRPrereq.h"
#include "GRSceneObjectComponent.h"

namespace GREngine
{
	class Viewport
	{
	public:
		Viewport(SceneObject *sceneObject);
		virtual ~Viewport();

		virtual void setColorTarget(const RenderTargetSPtr &renderTarget);
		virtual void setDepthTarget(const RenderTargetSPtr &depthTarget);

		const RenderTargetSPtr& colorTarget() const;
		const RenderTargetSPtr& depthTarget() const;

		const ViewportStateSPtr& state() const;

		GR_MEMORY_VIEW_ATTACH_INFO constantsMemory(MemoryRefSet &refs) const;
		void texturesRefs(MemoryRefSet &refs) const;

		SceneObject *sceneObject();

		void update();

		DirectX::XMMATRIX viewMatrix() const;
		DirectX::XMMATRIX projectionMatrix() const;

	private:
		ViewportStateSPtr createViewportState(const RenderTargetSPtr &renderTarget) const;

		void initGPUMemory();

	private:
		mutable std::mutex mGuard;

		RenderSystem *mRenderSystem;

		SceneObject *mSceneObject;

		RenderTargetSPtr mColorTarget;
		RenderTargetSPtr mDepthTarget;

		BufferSPtr mConstantsBuffer;

		ViewportStateSPtr mViewportState;

		DirectX::XMMATRIX mProjectionMatrix;
	};

	class ViewportState
	{
	public:
		ViewportState();
		~ViewportState();

		GR_MSAA_STATE_OBJECT msaa;
		GR_VIEWPORT_STATE_OBJECT viewport;
		GR_COLOR_BLEND_STATE_OBJECT colorBlend;
		GR_DEPTH_STENCIL_STATE_OBJECT depthStencil;
		GR_RASTER_STATE_OBJECT raster;
	};

	struct RenderViewportConstants
	{
		DirectX::XMMATRIX vMatrix;
		DirectX::XMMATRIX pMatrix;
	};
}