#pragma once

#include <mantle.h>

#include "GRPrereq.h"

namespace GREngine
{

	class Texture
	{
	public:
		static TextureSPtr createTexture(const std::string file);

	public:
		explicit Texture();
		explicit Texture(const GR_IMAGE_CREATE_INFO &imgInfo, GR_ENUM aspect, bool isTarget);
		virtual ~Texture();

		void setData(void *data, size_t size);

		virtual unsigned int width() const;
		virtual unsigned int height() const;

		const GPUMemorySPtr& gpuMemory() const;

		GR_IMAGE grImage() const;
		GR_IMAGE_SUBRESOURCE_RANGE subresource() const;

		GR_IMAGE_VIEW grView() const;

		void setState(GR_ENUM newState, bool blocking = false);
		GR_ENUM state() const;

		void clear(float *color);

	protected:
		mutable std::mutex mGuard;

		GR_IMAGE mImage;
		GR_IMAGE_CREATE_INFO mInfo;

		GR_ENUM mState;

		GR_IMAGE_SUBRESOURCE_RANGE mSubresource;

		GR_IMAGE_VIEW mView;

		GPUMemorySPtr mMemory;
	};

}