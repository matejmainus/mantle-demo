#pragma once

#include <vector>

#include "GRPrereq.h"
#include "GRSingleton.h"
#include "GRAttachment.h"

namespace GREngine
{
	class Log;

	class GRExport Kernel : public Singleton<Kernel>
	{
	public:
		Kernel(App *app);
		~Kernel();

		bool isRunning();
		void run();
		void stop();

		void addEventHandler(EventHandlerWPtr eventHandler);
		bool hasEventHandler(EventHandlerWPtr eventHandler);
		void removeEventHandler(EventHandlerWPtr eventHandler);

	private:
		void init();
		void shutdown();

	private:
		std::mutex mGuard;

		std::atomic<bool> mRun;

		App *mApp;

		Log *mLog;
		RenderSystem *mRenderSystem;
		SceneManager *mSceneManager;
		InputManager *mInputManager;

		Attachment<EventHandler> mEventHandlers;
	private:
		const int SLEEP_TIME = 1000 / 30;
	};

}

