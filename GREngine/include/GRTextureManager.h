#pragma once

#include "GRPrereq.h"
#include "GRSingleton.h"

struct FIBITMAP;

namespace GREngine
{ 
	class TextureManager : public Singleton<TextureManager>
	{
	public:
		explicit TextureManager();
		virtual ~TextureManager();

		TextureSPtr loadTexture(const std::string &fileName);

	private:
		RenderSystem *mRenderSystem;
	};

}