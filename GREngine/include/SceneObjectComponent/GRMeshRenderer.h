#pragma once

#include <mantle.h>

#include <set>
#include <DirectXMath.h>
#include <DirectXPackedVector.h>

#include "GRSceneObjectComponent.h"
#include "GRRenderer.h"

namespace GREngine
{
	namespace SceneObjectComponents
	{
		class MeshRenderCommand;

		class MeshRenderer : public SceneObjectComponent, public Renderer
		{
		public:
			MeshRenderer(SceneObject *obj);
			virtual ~MeshRenderer();

			virtual MeshSPtr& mesh() override;
			virtual MaterialSPtr& material() override;

			void setMesh(MeshSPtr mesh);
			void setMaterial(MaterialSPtr material);

			virtual bool canDraw() const override;
			virtual void draw(CommandBufferSPtr &commandBuffer, const RenderPhaseParamsSPtr &renderParams, const RenderContext &context) override;

			virtual void bindData(MemoryRefSet &memoryRefs, const DescriptorSetSPtr &descriptorSet, GR_UINT descriptorOffset,
				const RenderPhaseParamsSPtr &renderParams, const RenderContext &context) override;

			virtual void updateGPUMemory(CommandBufferSPtr &cmdBuff, const RenderPhaseParamsSPtr &renderParams, const RenderContext &context) override;
			virtual GR_MEMORY_VIEW_ATTACH_INFO objectConstantsView(MemoryRefSet &refs) const override;
			virtual GR_MEMORY_VIEW_ATTACH_INFO meshConstantsView(MemoryRefSet &refs) const override;

		private:
			void initGPUMemory();

		private:
			friend class MeshRenderCommand;

			mutable std::mutex mGuard;

			MeshSPtr mMesh;
			MaterialSPtr mMaterial;
			TransformSPtr mTransform;

			std::shared_ptr<MeshRenderCommand> mRenderCommand;

			BufferSPtr mConstantsBuffer;

		};

	}

}