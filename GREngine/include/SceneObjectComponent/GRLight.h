#pragma once

#include <DirectXMath.h>
#include <DirectXPackedVector.h>

#include "GRPrereq.h"
#include "GRSceneObjectComponent.h"

namespace GREngine
{
	namespace SceneObjectComponents
	{
		class Light : public SceneObjectComponent
		{
		public:
			Light(SceneObject *obj);
			virtual ~Light();

			void setColor(DirectX::XMVECTOR color);
			DirectX::XMVECTOR color() const;

			void setIntensity(float intensity);
			float intensity() const;

		private:
			mutable std::mutex mGuard;

			DirectX::XMVECTOR mColor;
			float mIntensity;
		};
	}
}