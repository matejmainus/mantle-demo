#pragma once

#include <mantle.h>

#include "GRPrereq.h"

namespace GREngine
{

	class Pipeline
	{
	public:
		Pipeline(const GR_GRAPHICS_PIPELINE_CREATE_INFO &info);
		virtual ~Pipeline();

		GR_PIPELINE grPipeline() const;

		const GPUMemorySPtr& gpuMemory() const;

		GR_UINT descriptorSlotCount() const;

	private:
		GR_PIPELINE mPipeline;

		GPUMemorySPtr mMemory;

		GR_UINT mSlotCount;
	};

}