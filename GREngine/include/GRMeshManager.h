#pragma once

#include <map>

#include "GRPrereq.h"
#include "GRSingleton.h"

namespace GREngine
{

	class MeshManager : public Singleton<MeshManager>
	{
	public:
		MeshManager();
		~MeshManager();

		MeshSPtr loadMesh(const std::string &path);

	private:
		std::mutex mMeshesMutex;

		typedef std::map<std::string, MeshWPtr> MeshesMap;

		MeshesMap mMeshes;
	};

}
