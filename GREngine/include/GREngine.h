#pragma once

#include <memory>

#define OBJECT_SPTR(TYPE) \
	typedef std::shared_ptr<TYPE> TYPE##SPtr

#define OBJECT_PTR(TYPE) \
	OBJECT_SPTR(TYPE); \
	typedef std::weak_ptr<TYPE> TYPE##WPtr

#define COLOR(val) \
	val / 255.0f

namespace GREngine
{
	class Kernel;
	class RenderSystem;
	class RenderSystemResourceManager;
	class TextureManager;
	class InputManager;
	class MeshManager;
	class Mesh;
	class GPUMemoryManager;
	class GPUMemory;
	class Buffer;
	class Scene;
	class CommandBuffer;
	class DescriptorSet;
	class RenderTree;
	class RenderWorker;
	class RenderThread;
	class RenderTarget;
	class Material;
	class Pipeline;
	class SceneManager;
	class Window;
	class Scene;
	class Texture;
	class MemoryRefSet;
	class Queue;
	class App;
	class SceneObject;
	class SceneObjectComponent;
	class EventHandler;
	class Renderer;
	class Viewport;
	class ViewportState;

	struct RenderSceneConstants;
	struct RenderMeshVertex;
	struct RenderObjectConstants;
	struct RenderViewportConstants;
	struct RenderContext;
	struct RenderPhaseParams;

	OBJECT_SPTR(App);
	OBJECT_SPTR(RenderTarget);
	OBJECT_SPTR(RenderWorker);
	OBJECT_SPTR(RenderThread);
	OBJECT_SPTR(RenderTree);
	OBJECT_SPTR(GPUMemory);
	OBJECT_PTR(Buffer);
	OBJECT_SPTR(DescriptorSet);
	OBJECT_PTR(Scene);
	OBJECT_SPTR(SceneObject);
	OBJECT_PTR(EventHandler);
	OBJECT_PTR(Mesh);
	OBJECT_SPTR(RenderPhaseParams);
	OBJECT_SPTR(Texture);
	OBJECT_SPTR(Material);
	OBJECT_SPTR(Pipeline);
	OBJECT_SPTR(Window);
	OBJECT_SPTR(SceneObjectComponent);
	OBJECT_SPTR(Renderer);
	OBJECT_SPTR(Viewport);
	OBJECT_SPTR(ViewportState);
	OBJECT_SPTR(CommandBuffer);

	namespace SceneObjectComponents
	{
		class Camera;
		class MeshRenderer;
		class Transform;
		class Light;

		OBJECT_SPTR(Camera);
		OBJECT_SPTR(MeshRenderer);
		OBJECT_SPTR(Transform);
		OBJECT_SPTR(Light);
	}

	namespace Materials
	{
		class SimpleMaterial;

		OBJECT_SPTR(SimpleMaterial);
	}
}