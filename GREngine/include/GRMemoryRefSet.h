#pragma once

#include <unordered_map>
#include <vector>
#include <initializer_list>
#include <mantle.h>

#include "GRPrereq.h"

namespace GREngine
{

typedef std::unordered_map<GR_GPU_MEMORY, GR_MEMORY_REF> ReferenceType;

class MemoryRefSet
{
public:
	explicit MemoryRefSet();
	~MemoryRefSet();

	void insert(const GR_MEMORY_REF &ref);
	void insert(const MemoryRefSet &refs);
	void insert(const std::initializer_list<GR_MEMORY_REF> &items);
	
	void clear();

	void items(std::vector<GR_MEMORY_REF> &refs) const;

private:
	void insert(const GR_MEMORY_REF* first, const GR_MEMORY_REF* last);

private:
	mutable std::mutex mGuard;

	ReferenceType mRefs;
};

}
