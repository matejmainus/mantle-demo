#pragma once

#include <mantle.h>

#include "GRPrereq.h"

namespace GREngine
{
	class GRExport RenderTarget
	{
	public:
		enum class Type
		{
			COLOR,
			DEPTH,
			STENCIL,
			DEPTH_STENCIL
		};

		explicit RenderTarget(const TextureSPtr& texture, Type type);
		virtual ~RenderTarget();

		const TextureSPtr& texture() const;

		virtual void clear();
		virtual void present();
		virtual void prepare();

		GR_COLOR_TARGET_VIEW colorTargetView() const;
		GR_DEPTH_STENCIL_VIEW depthStencilTargetView() const;

	protected:
		RenderSystem *mRenderSystem;

		TextureSPtr mTexture;

		GR_COLOR_TARGET_VIEW mColorTargetView;
		GR_DEPTH_STENCIL_VIEW mDepthStencilTargetView;
	};
}