#pragma once

#include <mantle.h>

#include "GRPrereq.h"

namespace GREngine
{
	class Renderer
	{
	public:

		virtual bool canDraw() const = 0;
		virtual void draw(CommandBufferSPtr &commandBuffer, const RenderPhaseParamsSPtr &renderParams, const RenderContext &context) = 0;

		virtual void bindData(MemoryRefSet &memoryRefs, const DescriptorSetSPtr &descriptorSet, GR_UINT descriptorOffset,
			const RenderPhaseParamsSPtr &renderParams, const RenderContext &context) = 0;

		virtual MeshSPtr& mesh() = 0;
		virtual MaterialSPtr& material() = 0;

		virtual void updateGPUMemory(CommandBufferSPtr &cmdBuff, const RenderPhaseParamsSPtr &renderParams, const RenderContext &context) = 0;
		
		virtual GR_MEMORY_VIEW_ATTACH_INFO objectConstantsView(MemoryRefSet &refs) const = 0;
		virtual GR_MEMORY_VIEW_ATTACH_INFO meshConstantsView(MemoryRefSet &refs) const = 0;
	};

}

