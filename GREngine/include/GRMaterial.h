#pragma once

#include <mantle.h>
#include <set>

#include "GRPrereq.h"

namespace GREngine
{
	class Material
	{
	public:
		Material();
		virtual ~Material();

		static std::string& name();

		bool isDirty() const;

		const PipelineSPtr& pipeline() const;

		virtual void updateGPUMemory(CommandBufferSPtr &cmdBuff, const RenderPhaseParamsSPtr &renderParams, const RenderContext &context) = 0;
		virtual GR_MEMORY_VIEW_ATTACH_INFO materialConstantsView(MemoryRefSet &refs) const = 0;

		virtual void bindData(MemoryRefSet &memoryRefs, const DescriptorSetSPtr &descriptorSet, GR_UINT descriptorOffset,
			const RenderPhaseParamsSPtr &renderParams, const RenderContext &context) = 0;

	protected:
		mutable std::mutex mGuard;

		PipelineSPtr mPipeline;

		bool mDirty;
	};

}