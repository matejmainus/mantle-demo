#pragma once

#include <map>
#include <set>
#include <vector>
#include <mantle.h>

#include <GRPrereq.h>
#include <GRSingleton.h>

namespace GREngine
{
	typedef std::map<std::string, PipelineSPtr> PipelinesMap;
	typedef std::map<std::string, GR_SHADER> ShadersMap;

	typedef const GR_GRAPHICS_PIPELINE_CREATE_INFO& (*pipelineCreateFunc) ();

	class RenderSystemResourceManager : public Singleton<RenderSystemResourceManager>
	{
	public:
		RenderSystemResourceManager();
		virtual ~RenderSystemResourceManager();

		RenderWorkerSPtr renderWorker();
		void returnRenderWorker(RenderWorkerSPtr renderWorker);

		CommandBufferSPtr getCmdBuffer(const GR_FLAGS buildFlags = 0, const GR_QUEUE_TYPE = GR_QUEUE_UNIVERSAL);
		void freeCommandBuffer(const CommandBufferSPtr command);

		DescriptorSetSPtr getDescriptorSet(const GR_UINT slotCount);
		void freeDescriptorSet(const DescriptorSetSPtr descriptorSet);

		GR_SHADER getShader(const std::string &filePath);

		PipelineSPtr getPipeline(const std::string &id, pipelineCreateFunc createFunc);

		void flush();

	private:
		void init();
		void shutdown();

		static std::vector<char> loadShaderFromFile(const std::string &filePath);

	private:
		mutable std::mutex mCmdBufMutex;
		mutable std::mutex mShadersMutex;
		mutable std::mutex mPipelinesMutex;
		mutable std::mutex mDescriptorSetsMutex;
		mutable std::mutex mRenderWorkersMutex;

		RenderSystem *mRenderSystem;

		std::set<CommandBufferSPtr> mFreeCmdBufs;
		std::set<CommandBufferSPtr> mQueuedCommands;

		std::multimap<GR_UINT, DescriptorSetSPtr> mFreeDescriptorSets;
		std::set<DescriptorSetSPtr> mQueuedDescriptorSets;

		std::set<RenderWorkerSPtr> mFreeRenderWorkers;

		ShadersMap mShaders;
		PipelinesMap mPipelines;
	};
}

