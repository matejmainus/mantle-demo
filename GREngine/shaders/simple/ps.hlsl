struct PIn {
	float4 position : SV_POSITION;
	float4 viewPos : TEXCOORD0;
	float4 normal : TEXCOORD1;
	float2 albedoUV : TEXCOORD2;
	float2 padding : TEXCOORD3;
};

struct SceneConstants
{
	unsigned int lightsCount;
	float3 padding;
};

struct Light
{
	float4 color;
	matrix mMatrix;
	float intensity;
	float3 padding;
};

struct MaterialConstants
{
	float4 color;
};

StructuredBuffer<SceneConstants> sceneConstants: register(t0);
StructuredBuffer<Light> lights : register(t1);
StructuredBuffer<MaterialConstants> materialConstants : register(t2);
Texture2D albedoTexture : register(t3);

SamplerState ColorSampler : register(s1);

float4 PShader(PIn input) : SV_TARGET {
    
	MaterialConstants mat = materialConstants[0];
	
	float4 color = albedoTexture.Sample(ColorSampler, input.albedoUV);
	color *= mat.color;

	Light light = lights[0];

	float4 lightPosition = mul(float4(0, 0, 20, 1.0f), light.mMatrix);

	float4 N = input.normal;
	float4 L = normalize(lightPosition - input.viewPos);

	float diffuse = max(dot(L, N), 0.0);
	color *= light.color * float4(diffuse, diffuse, diffuse, 1.0f) * light.intensity;

	return color;
}