struct VOut {
    float4 position : SV_POSITION;
	float4 viewPos : TEXCOORD0;
	float4 normal : TEXCOORD1;
	float2 albedoUV : TEXCOORD2;
	float2 padding : TEXCOORD3;
};

struct ViewportConstants
{
	matrix vMatrix;
	matrix pMatrix;
};

struct MeshVertex
{
	float4 pos;
	float4 normal;
	float2 uv;
	float2 padding;
};

struct ObjectConstants
{
	matrix mMatrix;
	matrix nMatrix;
};

StructuredBuffer<ViewportConstants> viewportConstants : register(t2);
StructuredBuffer<ObjectConstants> objectConstants : register(t3);
StructuredBuffer<MeshVertex> vertices : register(t4);

VOut VShader(uint id : SV_VertexID) {
    VOut output;

	ViewportConstants viewpConst = viewportConstants[0];
	ObjectConstants objConst = objectConstants[0];

	matrix mvp = mul(objConst.mMatrix, viewpConst.vMatrix);
	mvp = mul(mvp, viewpConst.pMatrix);

	MeshVertex ver = vertices[id];

	output.position = mul(ver.pos, mvp);
	output.albedoUV = ver.uv;
	output.viewPos = mul(float4(0.0f, 0.0f, 0.0f, 1.0f), viewpConst.vMatrix);
	output.normal = mul(ver.normal, objConst.nMatrix);

    return output;
}