#include <cmath>
#include <iostream>

#include "GRViewport.h"

#include "GRRenderSystem.h"
#include "GRRenderTarget.h"

#include "SceneObjectComponent\GRTransform.h"

#include "GRSceneObject.h"
#include "GRBuffer.h"
#include "GRGPUMemoryManager.h"
#include "GRGPUMemory.h"
#include "GRTexture.h"
#include "GRDescriptorSet.h"
#include "GRCommandBuffer.h"
#include "GRMemoryRefSet.h"

#define CHECK_AND_DESTROY(var) \
	if (var != GR_NULL_HANDLE) \
		grDestroyObject(var)

namespace GREngine
{
	using namespace SceneObjectComponents;

	Viewport::Viewport(SceneObject *sceneObject) :
		mSceneObject(sceneObject)
	{
		mRenderSystem = RenderSystem::getInstancePtr();

		initGPUMemory();
	}

	Viewport::~Viewport()
	{
	}

	void Viewport::setColorTarget(const RenderTargetSPtr &renderTarget)
	{
		MutexGuard locker(mGuard);

		mColorTarget = renderTarget;

		mProjectionMatrix = DirectX::XMMatrixPerspectiveFovLH(DirectX::XM_PIDIV4, 
			(float) renderTarget->texture()->width() / (float) renderTarget->texture()->height(),
			0.1f, 
			1000.0f);

		mViewportState = createViewportState(renderTarget);
	}

	void Viewport::setDepthTarget(const RenderTargetSPtr & depthTarget)
	{
		MutexGuard locker(mGuard);
		
		mDepthTarget = depthTarget;
	}

	const RenderTargetSPtr& Viewport::colorTarget() const
	{
		MutexGuard locker(mGuard);

		return mColorTarget;
	}

	const RenderTargetSPtr & Viewport::depthTarget() const
	{
		return mDepthTarget;
	}

	const ViewportStateSPtr& Viewport::state() const
	{
		MutexGuard locker(mGuard);

		return mViewportState;
	}

	GR_MEMORY_VIEW_ATTACH_INFO Viewport::constantsMemory(MemoryRefSet& refs) const
	{
		refs.insert(mConstantsBuffer->grReference(GR_MEMORY_REF_READ_ONLY));

		return mConstantsBuffer->grView({GR_CH_FMT_UNDEFINED, GR_NUM_FMT_UNDEFINED}, sizeof(RenderViewportConstants));
	}

	void Viewport::texturesRefs(MemoryRefSet& refs) const
	{
		refs.insert(mColorTarget->texture()->gpuMemory()->grReference());
		refs.insert(mDepthTarget->texture()->gpuMemory()->grReference());
	}

	SceneObject * Viewport::sceneObject()
	{
		return mSceneObject;
	}

	void Viewport::update()
	{
		DirectX::XMMATRIX vMatrix = viewMatrix();
		DirectX::XMMATRIX pMatrix = projectionMatrix();

		MutexGuard locker(mGuard);

		RenderViewportConstants constants;
		constants.vMatrix = DirectX::XMMatrixTranspose(vMatrix);
		constants.pMatrix = DirectX::XMMatrixTranspose(pMatrix);

		CommandBufferSPtr cmdBuff = CommandBuffer::getFromPool();
		MemoryRefSet refs;

		mConstantsBuffer->update(cmdBuff, sizeof(constants), &constants);
		mConstantsBuffer->setState(cmdBuff, GR_MEMORY_STATE_GRAPHICS_SHADER_READ_ONLY);

		refs.insert(mConstantsBuffer->grReference());
		cmdBuff->queue(refs);

		CommandBuffer::returnToPool(cmdBuff);
	}

	DirectX::XMMATRIX Viewport::viewMatrix() const
	{
		MutexGuard locker(mGuard);

		TransformSPtr transform = mSceneObject->component<Transform>();

		DirectX::XMVECTOR rotation = DirectX::XMVector3Rotate(DirectX::XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f), transform->rotation());

		DirectX::XMMATRIX viewMatrix = DirectX::XMMatrixLookToLH(
			DirectX::XMLoadFloat3(&transform->position()),
			rotation,
			DirectX::XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f));

		return viewMatrix;
	}

	DirectX::XMMATRIX Viewport::projectionMatrix() const
	{
		MutexGuard locker(mGuard);

		return mProjectionMatrix;
	}

	ViewportStateSPtr Viewport::createViewportState(const RenderTargetSPtr &renderTarget) const
	{
		GR_MSAA_STATE_CREATE_INFO msaaStateCreateInfo = {};
		msaaStateCreateInfo.samples = 1;
		msaaStateCreateInfo.sampleMask = 0xF; // RGBA bits

		GR_VIEWPORT_STATE_CREATE_INFO viewportStateCreateInfo = {};
		viewportStateCreateInfo.viewportCount = 1;
		viewportStateCreateInfo.scissorEnable = GR_FALSE;
		viewportStateCreateInfo.viewports[0].originX = 0;
		viewportStateCreateInfo.viewports[0].originY = 0;
		viewportStateCreateInfo.viewports[0].width = (GR_FLOAT) renderTarget->texture()->width();
		viewportStateCreateInfo.viewports[0].height = (GR_FLOAT) renderTarget->texture()->height();
		viewportStateCreateInfo.viewports[0].minDepth = 0;
		viewportStateCreateInfo.viewports[0].maxDepth = 1;

		GR_COLOR_BLEND_STATE_CREATE_INFO blendStateCreateInfo = {};
		blendStateCreateInfo.target[0].blendEnable = GR_FALSE;
		blendStateCreateInfo.target[0].srcBlendColor = GR_BLEND_SRC_ALPHA;
		blendStateCreateInfo.target[0].destBlendColor = GR_BLEND_ONE_MINUS_SRC_ALPHA;
		blendStateCreateInfo.target[0].blendFuncColor = GR_BLEND_FUNC_ADD;

		blendStateCreateInfo.target[0].srcBlendAlpha = GR_BLEND_ONE;
		blendStateCreateInfo.target[0].destBlendAlpha = GR_BLEND_ONE;
		blendStateCreateInfo.target[0].blendFuncAlpha = GR_BLEND_FUNC_ADD;

		GR_DEPTH_STENCIL_STATE_CREATE_INFO depthStencilStateCreateInfo = {};
		depthStencilStateCreateInfo.depthEnable = GR_TRUE;
		depthStencilStateCreateInfo.depthWriteEnable = GR_TRUE;
		depthStencilStateCreateInfo.stencilEnable = GR_FALSE;
		depthStencilStateCreateInfo.minDepth = 0;
		depthStencilStateCreateInfo.maxDepth = 1;
		depthStencilStateCreateInfo.depthBoundsEnable = GR_TRUE;
		depthStencilStateCreateInfo.depthFunc = GR_COMPARE_LESS;

		depthStencilStateCreateInfo.front.stencilDepthFailOp = GR_STENCIL_OP_KEEP;
		depthStencilStateCreateInfo.front.stencilFailOp = GR_STENCIL_OP_KEEP;
		depthStencilStateCreateInfo.front.stencilPassOp = GR_STENCIL_OP_KEEP;
		depthStencilStateCreateInfo.front.stencilFunc = GR_COMPARE_ALWAYS;
		depthStencilStateCreateInfo.front.stencilRef = 0;

		depthStencilStateCreateInfo.back.stencilDepthFailOp = GR_STENCIL_OP_KEEP;
		depthStencilStateCreateInfo.back.stencilFailOp = GR_STENCIL_OP_KEEP;
		depthStencilStateCreateInfo.back.stencilPassOp = GR_STENCIL_OP_KEEP;
		depthStencilStateCreateInfo.back.stencilFunc = GR_COMPARE_ALWAYS;
		depthStencilStateCreateInfo.back.stencilRef = 0;

		GR_RASTER_STATE_CREATE_INFO rasterStateCreateInfo = {};
		rasterStateCreateInfo.fillMode = GR_FILL_SOLID;
		rasterStateCreateInfo.cullMode = GR_CULL_BACK;
		rasterStateCreateInfo.frontFace = GR_FRONT_FACE_CW;

		ViewportStateSPtr viewportState = std::make_shared<ViewportState>();

		grCreateMsaaState(mRenderSystem->device(), &msaaStateCreateInfo, &viewportState->msaa);
		grCreateViewportState(mRenderSystem->device(), &viewportStateCreateInfo, &viewportState->viewport);
		grCreateColorBlendState(mRenderSystem->device(), &blendStateCreateInfo, &viewportState->colorBlend);
		grCreateDepthStencilState(mRenderSystem->device(), &depthStencilStateCreateInfo, &viewportState->depthStencil);
		grCreateRasterState(mRenderSystem->device(), &rasterStateCreateInfo, &viewportState->raster);

		return viewportState;
	}

	void Viewport::initGPUMemory()
	{
		GPUMemorySPtr mConstantsMemory = GPUMemoryManager::getInstance().allocateGPUMemory(sizeof(RenderViewportConstants), GR_MEMORY_HEAP_CPU_UNCACHED);
		mConstantsBuffer = std::make_shared<Buffer>(mConstantsMemory, sizeof(RenderViewportConstants));
	}

	ViewportState::ViewportState()
	{
	}

	ViewportState::~ViewportState()
	{
		CHECK_AND_DESTROY(msaa);
		CHECK_AND_DESTROY(viewport);
		CHECK_AND_DESTROY(colorBlend);
		CHECK_AND_DESTROY(depthStencil);
		CHECK_AND_DESTROY(raster);
	}

}