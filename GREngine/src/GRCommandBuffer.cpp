#include "GRCommandBuffer.h"
#include "GRRenderSystem.h"
#include "GRQueue.h"
#include "GRRenderSystemResourceManager.h"

#include <cstdarg>

namespace GREngine
{

	CommandBufferSPtr CommandBuffer::getFromPool(const GR_FLAGS buildFlags, const GR_QUEUE_TYPE queueType)
	{
		return RenderSystemResourceManager::getInstance().getCmdBuffer(buildFlags, queueType);
	}

	void CommandBuffer::returnToPool(CommandBufferSPtr cmdBuff)
	{
		RenderSystemResourceManager::getInstance().freeCommandBuffer(cmdBuff);
	}

	CommandBuffer::CommandBuffer(const GR_CMD_BUFFER_CREATE_INFO &info):
		mIsInRecState(false)
	{
		const RenderSystem &renderSystem = RenderSystem::getInstance();

		grCreateCommandBuffer(renderSystem.device(), &info, &mCommandBuffer);
	}

	CommandBuffer::~CommandBuffer()
	{
		grDestroyObject(mCommandBuffer);
	}

	GR_CMD_BUFFER CommandBuffer::grCmdBuffer() const
	{
		return mCommandBuffer;
	}

	void CommandBuffer::end()
	{
		MutexGuard locker(mGuard);

		if(mIsInRecState)
		{
			grEndCommandBuffer(mCommandBuffer);
			mIsInRecState = false;
		}
	}

	void CommandBuffer::reset()
	{
		MutexGuard locker(mGuard);

		if (mIsInRecState)
		{
			grResetCommandBuffer(mCommandBuffer);

			mIsInRecState = false;
		}
	}

	void CommandBuffer::begin(const GR_FLAGS buildFlags)
	{
		MutexGuard locker(mGuard);

		if (!mIsInRecState)
		{		
			grBeginCommandBuffer(mCommandBuffer, buildFlags);

			mIsInRecState = true;
		}

	}

	void CommandBuffer::queue(const MemoryRefSet &memRefs)
	{
		end();

		RenderSystem::getInstance().universalQueue().queueCommand(mCommandBuffer, memRefs);
	}

	void CommandBuffer::queueBlocking(const MemoryRefSet &memRefs)
	{
		end();

		RenderSystem::getInstance().universalQueue().queueCommandBlocking(mCommandBuffer, memRefs);
	}
}