#include <SDL.h>
#include <SDL_syswm.h>

#include "GRWindow.h"
#include "GRRenderSystem.h"
#include "GRGPUMemory.h"
#include "GRQueue.h"
#include "GRRenderSystemResourceManager.h"
#include "GRLog.h"
#include "GRTexture.h"

namespace GREngine
{

class WSITexture : public Texture
{
public:
	WSITexture(unsigned int width, unsigned int height) :
		mWidth(width),
		mHeight(height)
	{
		RenderSystem &renderSystem = RenderSystem::getInstance();

		mState = GR_IMAGE_STATE_UNINITIALIZED;

		mSubresource.aspect = GR_IMAGE_ASPECT_COLOR;
		mSubresource.baseMipLevel = 0;
		mSubresource.mipLevels = 1;
		mSubresource.baseArraySlice = 0;
		mSubresource.arraySize = 1;

		GR_WSI_WIN_PRESENTABLE_IMAGE_CREATE_INFO wsiImageCreateInfo = {};
		wsiImageCreateInfo.usage = GR_IMAGE_USAGE_COLOR_TARGET;
		wsiImageCreateInfo.extent = { (GR_INT)width, (GR_INT)height };
		wsiImageCreateInfo.format = {
			GR_CH_FMT_R8G8B8A8,
			GR_NUM_FMT_UNORM
		};

		GR_GPU_MEMORY imageMemory;

		grWsiWinCreatePresentableImage(renderSystem.device(), &wsiImageCreateInfo, &mImage, &imageMemory);

		mMemory = std::make_shared<GPUMemory>(imageMemory);
	}

	virtual unsigned int width() const override
	{
		return mWidth;
	}

	virtual unsigned int height() const override
	{
		return mHeight;
	}

private:
	unsigned int mWidth;
	unsigned int mHeight;
};


Window::Window(const std::string &name, unsigned int width, unsigned int height) :
	RenderTarget(std::make_shared<WSITexture>(width, height), RenderTarget::Type::COLOR),
	mPresentInfo({})
{
	mWindow = SDL_CreateWindow(name.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, 0);

	SDL_SysWMinfo windowInfo;
	SDL_VERSION(&windowInfo.version);

	if (!SDL_GetWindowWMInfo(mWindow, &windowInfo))
		throw new std::runtime_error("Could not get window info");

	mPresentInfo.hWndDest = windowInfo.info.win.window;
	mPresentInfo.srcImage = mTexture->grImage();
	mPresentInfo.presentMode = GR_WSI_WIN_PRESENT_MODE_WINDOWED;

	Log::info("Render target window created name: " + name);
}

Window::~Window()
{
	SDL_DestroyWindow(mWindow);
}

bool Window::handleEvent(const SDL_Event * event)
{
	switch (event->type)
	{
	case SDL_WINDOWEVENT:
		return handleWindowEvent(&event->window);
	default:
		return false;
	}
}

void Window::present()
{
	mTexture->setState(GR_WSI_WIN_IMAGE_STATE_PRESENT_WINDOWED);

	mRenderSystem->universalQueue().wsiPresent(&mPresentInfo);
}

bool Window::handleWindowEvent(const SDL_WindowEvent * event)
{
	return event->windowID == SDL_GetWindowID(mWindow);
}

}