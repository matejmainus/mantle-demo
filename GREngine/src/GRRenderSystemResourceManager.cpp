#include <algorithm>

#include "GRRenderSystemResourceManager.h"

#include "GRRenderSystem.h"
#include "GRLog.h"

#include "GRCommandBuffer.h"
#include "GRDescriptorSet.h"
#include "GRPipeline.h"
#include "GRRenderWorker.h"

namespace GREngine
{

RenderSystemResourceManager::RenderSystemResourceManager()
{
	Log::info("RenderSystemResources created");

	mRenderSystem = RenderSystem::getInstancePtr();

	init();
}

RenderSystemResourceManager::~RenderSystemResourceManager()
{
	shutdown();

	mRenderSystem = 0;

	Log::info("RenderSystemResources destroyed");
}

void RenderSystemResourceManager::init()
{

}

void RenderSystemResourceManager::shutdown()
{

	mFreeCmdBufs.clear();
	mFreeDescriptorSets.clear();

	mShaders.clear();
	mPipelines.clear();
}

CommandBufferSPtr RenderSystemResourceManager::getCmdBuffer(const GR_FLAGS buildFlags, const GR_QUEUE_TYPE type)
{
	MutexGuard locker(mCmdBufMutex);

	CommandBufferSPtr cmdBuf;

	if(mFreeCmdBufs.empty())
	{
		GR_CMD_BUFFER_CREATE_INFO createInfo = { (GR_ENUM) type, 0 };

		cmdBuf = std::make_shared<CommandBuffer>(createInfo);
	}
	else
	{
		cmdBuf = *(mFreeCmdBufs.begin());
		mFreeCmdBufs.erase(mFreeCmdBufs.begin());
	}

	cmdBuf->begin();

	return cmdBuf;
}

void RenderSystemResourceManager::freeCommandBuffer(const CommandBufferSPtr command)
{
	MutexGuard locker(mCmdBufMutex);

	mQueuedCommands.insert(command);
}

DescriptorSetSPtr RenderSystemResourceManager::getDescriptorSet(const GR_UINT slotCount)
{
	MutexGuard locker(mDescriptorSetsMutex);

	DescriptorSetSPtr desc;

	std::multimap<GR_UINT, DescriptorSetSPtr>::iterator it = mFreeDescriptorSets.find(slotCount);

	if (it == mFreeDescriptorSets.end())
	{
		GR_DESCRIPTOR_SET_CREATE_INFO createInfo = {};
		createInfo.slots = slotCount;

		desc = std::make_shared<DescriptorSet>(createInfo);
	}
	else
	{
		desc = it->second;
		mFreeDescriptorSets.erase(it);
	}

	return desc;
}

void RenderSystemResourceManager::freeDescriptorSet(const DescriptorSetSPtr descriptorSet)
{
	MutexGuard locker(mDescriptorSetsMutex);

	mQueuedDescriptorSets.insert(descriptorSet);
}

RenderWorkerSPtr RenderSystemResourceManager::renderWorker()
{
	MutexGuard locker(mRenderWorkersMutex);

	RenderWorkerSPtr worker;

	if (mFreeRenderWorkers.empty())
	{
		worker = std::make_shared<RenderWorker>();
	}
	else
	{
		worker = *(mFreeRenderWorkers.begin());
		mFreeRenderWorkers.erase(worker);
	}

	return worker;
}

void RenderSystemResourceManager::returnRenderWorker(RenderWorkerSPtr renderWorker)
{
	MutexGuard locker(mRenderWorkersMutex);

	mFreeRenderWorkers.insert(renderWorker);
}

GR_SHADER RenderSystemResourceManager::getShader(const std::string & filePath)
{
	MutexGuard locker(mShadersMutex);

	RenderSystem *rs = RenderSystem::getInstancePtr();

	ShadersMap::iterator it = mShaders.find(filePath);
	if (it != mShaders.end())
		return it->second;

	GR_SHADER shader = GR_NULL_HANDLE;
	GR_SHADER_CREATE_INFO shaderCreateInfo = {};

	std::vector<char> code = loadShaderFromFile(filePath);

	shaderCreateInfo.pCode = code.data();
	shaderCreateInfo.codeSize = code.size();

	grCreateShader(rs->device(), &shaderCreateInfo, &shader);

	mShaders[filePath] = shader;

	return shader;
}

PipelineSPtr RenderSystemResourceManager::getPipeline(const std::string &id, pipelineCreateFunc createFunc)
{
	MutexGuard locker(mPipelinesMutex);

	PipelinesMap::iterator it = mPipelines.find(id);
	if (it != mPipelines.end())
		return it->second;

	const GR_GRAPHICS_PIPELINE_CREATE_INFO &info = createFunc();

	PipelineSPtr ptr = std::make_shared<Pipeline>(info);
	mPipelines[id] = ptr;

	return ptr;
}

void RenderSystemResourceManager::flush()
{
	MutexGuard locker(mCmdBufMutex);

	for (CommandBufferSPtr buffer : mQueuedCommands)
	{
		buffer->reset();
		mFreeCmdBufs.insert(buffer);
	}

	for (DescriptorSetSPtr descriptorSet : mQueuedDescriptorSets)
	{
		descriptorSet->reset();
		mFreeDescriptorSets.insert(std::pair<GR_UINT, DescriptorSetSPtr>(descriptorSet->slotCount(), descriptorSet));
	}

	mQueuedCommands.clear();
	mQueuedDescriptorSets.clear();
}

std::vector<char> RenderSystemResourceManager::loadShaderFromFile(const std::string & filePath)
{
	std::streampos size;
	std::vector<char> data;

	std::ifstream file(filePath, std::ios::in | std::ios::binary | std::ios::ate);
	if (file.is_open())
	{
		size = file.tellg();
		data.resize(size);

		file.seekg(0, std::ios::beg);
		file.read(&data[0], size);
		file.close();

		return data;
	}
	else
		throw new std::runtime_error("Could not open shader file: " + filePath);
}


}