#include "GRBuffer.h"

#include "GRCommandBuffer.h"
#include "GRGPUMemory.h"

namespace GREngine
{

Buffer::Buffer(const GPUMemorySPtr & memory, GPUMemory::ObjectAllocInfo & allocInfo) :
	mMemory(memory)
{
	mInfo = {};
	mInfo.mem = mMemory->grMemory();
	mInfo.range = allocInfo.range;
	mInfo.offset = allocInfo.offset;
	mInfo.state = GR_MEMORY_STATE_DATA_TRANSFER;
}

Buffer::Buffer(const GPUMemorySPtr &gpuMemory, const GR_GPU_SIZE range, const GR_GPU_SIZE alignment) :
	mMemory(gpuMemory)
{
	GPUMemory::ObjectAllocInfo allocInfo;
	bool succ = gpuMemory->suballocate(range, allocInfo);

	if (!succ)
		throw new std::runtime_error("Not enought free space in GPU memory for buffer");

	mInfo = {};
	mInfo.mem = mMemory->grMemory();
	mInfo.range = allocInfo.range;
	mInfo.offset = allocInfo.offset;
	mInfo.state = GR_MEMORY_STATE_DATA_TRANSFER;
}

Buffer::~Buffer()
{
	GPUMemory::ObjectAllocInfo allocInfo = {};
	allocInfo.offset = mInfo.offset;
	allocInfo.range = mInfo.range;

	mMemory->desuballocate(allocInfo);
}

GR_GPU_SIZE Buffer::range() const
{
	return mInfo.range;
}

GR_GPU_SIZE Buffer::offset() const
{
	return mInfo.offset;
}

const GPUMemorySPtr& Buffer::gpuMemory() const
{
	return mMemory;
}

GR_MEMORY_VIEW_ATTACH_INFO Buffer::grView(const GR_FORMAT format, const GR_GPU_SIZE stride, const GR_GPU_SIZE offset, const GR_GPU_SIZE range) const
{
	GR_MEMORY_VIEW_ATTACH_INFO info = mInfo;
	info.format = format;
	info.stride = stride;

	if (offset > 0)
		info.offset = offset;

	if (range > 0)
		info.range = range;

	return info;
}

GR_MEMORY_REF Buffer::grReference(const GR_ENUM flags) const
{
	return mMemory->grReference(flags);
}

void Buffer::update(size_t dataSize, void * data, const GR_GPU_SIZE offset)
{
	CommandBufferSPtr commandBuff = CommandBuffer::getFromPool(GR_CMD_BUFFER_OPTIMIZE_ONE_TIME_SUBMIT);

	update(commandBuff, dataSize, data, offset);

	MemoryRefSet refsSet;
	refsSet.insert(mMemory->grReference());

	commandBuff->queue(refsSet);

	CommandBuffer::returnToPool(commandBuff);
}

void Buffer::update(CommandBufferSPtr & cmdBuff, size_t dataSize, void * data, const GR_GPU_SIZE offset)
{
	GR_CMD_BUFFER grCmdBuff = cmdBuff->grCmdBuffer();

	setState(cmdBuff, GR_MEMORY_STATE_DATA_TRANSFER);

	grCmdUpdateMemory(grCmdBuff, mMemory->grMemory(), mInfo.offset + offset, dataSize, (GR_UINT32 *)data);
}

bool Buffer::setState(const GR_ENUM newState, bool blocking)
{
	CommandBufferSPtr cmdBuff = CommandBuffer::getFromPool(GR_CMD_BUFFER_OPTIMIZE_ONE_TIME_SUBMIT);

	bool changed = setState(cmdBuff, newState);

	if (changed)
	{
		MemoryRefSet refsSet;
		refsSet.insert(mMemory->grReference());

		if (blocking)
			cmdBuff->queueBlocking(refsSet);
		else
			cmdBuff->queue(refsSet);
	}

	CommandBuffer::returnToPool(cmdBuff);

	return changed;
}

bool Buffer::setState(CommandBufferSPtr & cmdBuff, const GR_ENUM newState)
{
	MutexGuard locker(mGuard);

	if (mInfo.state == newState)
		return false;

	GR_MEMORY_STATE_TRANSITION stateTransition = {};
	stateTransition.mem = mMemory->grMemory();
	stateTransition.offset = mInfo.offset;
	stateTransition.regionSize = mInfo.range;

	GR_CMD_BUFFER grCmdBuff = cmdBuff->grCmdBuffer();

	stateTransition.oldState = mInfo.state;
	stateTransition.newState = newState;

	grCmdPrepareMemoryRegions(grCmdBuff, 1, &stateTransition);

	mInfo.state = newState;

	return true;
}


}