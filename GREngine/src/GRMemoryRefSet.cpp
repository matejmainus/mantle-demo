#include "GRMemoryRefSet.h"

namespace GREngine
{

	MemoryRefSet::MemoryRefSet()
	{
	}

	MemoryRefSet::~MemoryRefSet()
	{
	}

	void MemoryRefSet::insert(const GR_MEMORY_REF& ref)
	{
		MutexGuard locker(mGuard);

		ReferenceType::iterator it = mRefs.find(ref.mem);

		if (it != mRefs.end())
			it->second.flags &= ref.flags;
		else
			mRefs[ref.mem] = ref;
	}

	void MemoryRefSet::insert(const MemoryRefSet & refs)
	{
		for (const std::pair<GR_GPU_MEMORY, GR_MEMORY_REF> &ref : refs.mRefs)
		{
			insert(ref.second);
		}
	}

	void MemoryRefSet::insert(const GR_MEMORY_REF * first, const GR_MEMORY_REF * last)
	{
		for (const GR_MEMORY_REF *it = first; it < last; ++it)
		{
			insert(*it);
		}
	}

	void MemoryRefSet::insert(const std::initializer_list<GR_MEMORY_REF> &items)
	{
		insert(items.begin(), items.end());
	}

	void MemoryRefSet::clear()
	{
		MutexGuard locker(mGuard);

		mRefs.clear();
	}

	void MemoryRefSet::items(std::vector<GR_MEMORY_REF> &refs) const
	{
		MutexGuard locker(mGuard);

		for (const std::pair<GR_GPU_MEMORY, GR_MEMORY_REF> &ref : mRefs)
			refs.push_back(ref.second);
	}
}