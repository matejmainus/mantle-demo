#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "GRMeshManager.h"

#include "GRMesh.h"

namespace GREngine
{

MeshManager::MeshManager()
{
}

MeshManager::~MeshManager()
{
}

MeshSPtr MeshManager::loadMesh(const std::string & path)
{
	MutexGuard locker(mMeshesMutex);

	MeshSPtr mesh;

	MeshesMap::iterator it = mMeshes.find(path);
	if (it != mMeshes.end())
	{
		mesh = it->second.lock();
		if (mesh)
			return mesh;
	}
	
	Assimp::Importer importer;

	const aiScene* scene = importer.ReadFile(path,
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType | 
		aiProcess_GenNormals | 
		aiProcess_GenUVCoords);

	importer.GetOrphanedScene();

	if (!scene)
		throw std::runtime_error(importer.GetErrorString());
	
	mesh = std::make_shared<Mesh>(scene->mMeshes[0]);

	mMeshes[path] = mesh;

	return mesh;
}

}