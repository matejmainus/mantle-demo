#include "SceneObjectComponent\GRTransform.h"

namespace GREngine
{

namespace SceneObjectComponents
{

	using namespace DirectX;

	Transform::Transform(SceneObject *obj) :
		SceneObjectComponent(obj)
	{
		mPosition = { 0.0f, 0.0f, 0.0f, 0.0f };
		mRotation = XMQuaternionIdentity();
		mScale = { 1.0f, 1.0f, 1.0f, 0.0f };
	}

	Transform::~Transform()
	{
	}

	void Transform::moveTo(XMFLOAT3 vec)
	{
		MutexGuard locker(mGuard);

		mPosition = XMLoadFloat3(&vec);
	}

	void Transform::rotateToAngles(DirectX::XMFLOAT3 angles)
	{
		MutexGuard locker(mGuard);

		mRotation = XMQuaternionRotationRollPitchYaw(
			XMConvertToRadians(angles.x),
			XMConvertToRadians(angles.y),
			XMConvertToRadians(angles.z));
	}

	void Transform::rotateToRadians(DirectX::XMFLOAT3 rads)
	{
		MutexGuard locker(mGuard);

		mRotation = XMQuaternionRotationRollPitchYaw(rads.x, rads.y, rads.z);
	}

	void Transform::rotateTo(XMVECTOR quat)
	{
		MutexGuard locker(mGuard);

		mRotation = quat;
	}

	void Transform::scaleTo(XMFLOAT3 ratio)
	{
		MutexGuard locker(mGuard);

		mScale = XMLoadFloat3(&ratio);
	}

	void Transform::move(XMFLOAT3 vec)
	{
		MutexGuard locker(mGuard);

		mPosition = XMVectorAdd(mPosition, XMLoadFloat3(&vec));
	}

	void Transform::rotate(DirectX::XMVECTOR quat)
	{
		MutexGuard locker(mGuard);

		mRotation = XMQuaternionMultiply(mRotation, quat);
	}

	void Transform::rotateByAngles(DirectX::XMFLOAT3 angles)
	{
		XMVECTOR quat = XMQuaternionRotationRollPitchYaw(
			XMConvertToRadians(angles.x),
			XMConvertToRadians(angles.y), 
			XMConvertToRadians(angles.z));

		rotate(quat);
	}

	void Transform::rotateByRadians(XMFLOAT3 rads)
	{
		XMVECTOR quat = XMQuaternionRotationRollPitchYaw(rads.x, rads.y, rads.z);

		rotate(quat);
	}

	void Transform::scale(XMFLOAT3 ratio)
	{
		MutexGuard locker(mGuard);

		mScale = XMVectorAdd(mScale, XMLoadFloat3(&ratio));
	}

	XMFLOAT3 Transform::position() const
	{
		MutexGuard locker(mGuard);

		XMFLOAT3 ret;
		XMStoreFloat3(&ret, mPosition);

		return ret;
	}

	XMVECTOR Transform::rotation() const
	{
		MutexGuard locker(mGuard);

		return mRotation;
	}

	XMFLOAT3 Transform::scale() const
	{
		MutexGuard locker(mGuard);

		XMFLOAT3 ret;
		XMStoreFloat3(&ret, mScale);

		return ret;
	}

	XMMATRIX Transform::matrix() const
	{
		MutexGuard locker(mGuard);

		XMVECTOR origin = { 0.0f, 0.0f, 0.0f, 0.0f };

		return XMMatrixAffineTransformation(
			mScale, origin, mRotation, mPosition);
	}
}

}

