#include "SceneObjectComponent/GRCamera.h"
#include "GRSceneObject.h"
#include "GRScene.h"
#include "GRRenderTarget.h"
#include "GRTexture.h"

namespace GREngine
{
namespace SceneObjectComponents
{

Camera::Camera(SceneObject *sceneObject) :
	Viewport(sceneObject),
	SceneObjectComponent(sceneObject)
{
}

Camera::~Camera()
{
}

void Camera::setRenderTarget(const RenderTargetSPtr &colorTarget)
{
	GR_IMAGE_CREATE_INFO depthInfo = {};
	depthInfo.extent.width = colorTarget->texture()->width();
	depthInfo.extent.height = colorTarget->texture()->height();
	depthInfo.extent.depth = 1;
	depthInfo.format.channelFormat = GR_CH_FMT_R32G8;
	depthInfo.format.numericFormat = GR_NUM_FMT_DS;
	depthInfo.imageType = GR_IMAGE_2D;
	depthInfo.arraySize = 1;
	depthInfo.mipLevels = 1;
	depthInfo.samples = 1;
	depthInfo.tiling = GR_OPTIMAL_TILING;
	depthInfo.usage = GR_IMAGE_USAGE_DEPTH_STENCIL;

	depthTexture = std::make_shared<Texture>(depthInfo, GR_IMAGE_ASPECT_DEPTH, true);

	depthTarget = std::make_shared<RenderTarget>(depthTexture, RenderTarget::Type::DEPTH);

	Viewport::setColorTarget(colorTarget);
	Viewport::setDepthTarget(depthTarget);
}

void Camera::update(int timeInMilis)
{
}


}
}