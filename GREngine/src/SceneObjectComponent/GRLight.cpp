#include "SceneObjectComponent\GRLight.h"
#include "GRSceneObject.h"

namespace GREngine
{
namespace SceneObjectComponents
{

Light::Light(SceneObject * obj) :
	SceneObjectComponent(obj),
	mColor({255.0f, 255.0f, 255.0f, 255.0f}),
	mIntensity(1.0f)
{}

Light::~Light()
{}

void Light::setColor(DirectX::XMVECTOR color)
{
	MutexGuard locker(mGuard);

	mColor = color;
}

DirectX::XMVECTOR Light::color() const
{
	MutexGuard locker(mGuard);

	return mColor;
}

void Light::setIntensity(float intensity)
{
	MutexGuard locker(mGuard);

	mIntensity = intensity;
}

float Light::intensity() const
{
	MutexGuard locker(mGuard);

	return mIntensity;
}

}
}
