
#include "GRInputManager.h"

namespace GREngine
{

	InputManager::InputManager()
	{
	}


	InputManager::~InputManager()
	{
	}

	bool InputManager::keyState(SDL_Scancode keyCode) const
	{
		const Uint8 *mask = SDL_GetKeyboardState(nullptr);

		return mask[keyCode] == 1;
	}

	bool InputManager::mouseButtonState(unsigned char buttonCode) const
	{
		Uint32 mask = SDL_GetMouseState(nullptr, nullptr);

		return (mask & SDL_BUTTON(buttonCode)) == 1;
	}

	void InputManager::mousePosition(int *x, int *y) const
	{
		SDL_GetMouseState(x, y);
	}

	void InputManager::mouseRelativePosition(int * x, int * y) const
	{
		SDL_GetRelativeMouseState(x, y);
	}

}