#include <SDL.h>
#include <algorithm>
#include <stdexcept>

#include "GRKernel.h"
#include "GRLog.h"
#include "GRRenderSystem.h"
#include "GRSceneManager.h"
#include "GRInputManager.h"
#include "GRApp.h"
#include "GREventHandler.h"

namespace GREngine
{

Kernel::Kernel(App *app) :
	mRun(true),
	mApp(app)
{
	mLog = new Log();

	init();

	mRenderSystem = new RenderSystem();
	mSceneManager = new SceneManager();
	mInputManager = new InputManager();

	Log::info("Kernel created");

	mApp->init();
}

Kernel::~Kernel()
{
	mApp->shutdown();

	shutdown();

	delete mInputManager;
	delete mSceneManager;
	delete mRenderSystem;

	Log::info("Kernel destroyed");
	delete mLog;
}

void Kernel::init()
{
	if (SDL_Init(SDL_INIT_TIMER | SDL_INIT_EVENTS) != 0)
		throw std::runtime_error("Could not init SDL");
}

void Kernel::shutdown()
{
	mEventHandlers.clear();

	SDL_Quit();
}

void Kernel::addEventHandler(EventHandlerWPtr eventHandler)
{
	return mEventHandlers.attach(eventHandler);
}

bool Kernel::hasEventHandler(EventHandlerWPtr eventHandler)
{
	return mEventHandlers.isAttached(eventHandler);
}

void Kernel::removeEventHandler(EventHandlerWPtr eventHandler)
{
	mEventHandlers.detach(eventHandler);
}

bool Kernel::isRunning()
{
	return mRun;
}

void Kernel::run()
{
	unsigned int lastTime, currentTime, deltaTime = 0;
	lastTime = currentTime = SDL_GetTicks();

	mApp->start();

	while (isRunning())
	{
		std::set<EventHandlerSPtr> eventHandlersCopy = mEventHandlers.items();

		SDL_Event event;
		while (SDL_WaitEventTimeout(&event, SLEEP_TIME))
		{
			if (event.type == SDL_QUIT) 
			{
				mRun = false;
				break;
			}
			
			for (const EventHandlerSPtr &eventHandler : eventHandlersCopy)
			{
				eventHandler->handleEvent(&event);
			}
		}

		currentTime = SDL_GetTicks();
		deltaTime = currentTime - lastTime;
		lastTime = currentTime;

		mApp->update(deltaTime);
	}

	mApp->stop();
}

void Kernel::stop()
{
	mRun = false;
}

}