#include "Material\GRSimpleMaterial.h"

#include "GRRenderSystem.h"
#include "GRGPUMemoryManager.h"
#include "GRGPUMemory.h"
#include "GRRenderSystemResourceManager.h"
#include "GRTexture.h"
#include "GRPipeline.h"
#include "GRBuffer.h"
#include "GRRenderer.h"
#include "GRRenderWorker.h"
#include "GRDescriptorSet.h"
#include "GRScene.h"
#include "GRViewport.h"
#include "GRRenderTree.h"
#include "GRCommandBuffer.h"
#include "GRMemoryRefSet.h"

#define DESCRIPTOR_SLOT(var, id, type, pos) \
	var[id] = {}; \
	var[id].slotObjectType = type; \
	var[id].shaderEntityIndex = pos; 

namespace GREngine
{

namespace Materials
{

const int DESCRIPTOR_SIZE = 8;

GR_DESCRIPTOR_SLOT_INFO vsDescriptorSlots[DESCRIPTOR_SIZE];
GR_DESCRIPTOR_SLOT_INFO psDescriptorSlots[DESCRIPTOR_SIZE];

GR_GRAPHICS_PIPELINE_CREATE_INFO pipelineCreateInfo = {};

SimpleMaterialSPtr SimpleMaterial::create(DirectX::XMVECTOR color, TextureSPtr albedoTexture)
{
	return std::make_shared<SimpleMaterial>(color, albedoTexture);
}

SimpleMaterial::SimpleMaterial(DirectX::XMVECTOR color, TextureSPtr albedoTexture) :
	Material(),
	mMaterialConstants({}),
	mAlbedoTexture(albedoTexture)
{
	setColor(color);

	initGPUMemory();

	createPipeline();
	createSampler();
}

SimpleMaterial::~SimpleMaterial()
{
}

void SimpleMaterial::setColor(DirectX::XMVECTOR color)
{
	MutexGuard locker(mGuard);

	DirectX::XMStoreFloat4(&mMaterialConstants.color, color);

	mDirty = true;
}

GR_MEMORY_VIEW_ATTACH_INFO SimpleMaterial::materialConstantsView(MemoryRefSet& refs) const
{
	refs.insert(mConstantsBuffer->grReference());

	return mConstantsBuffer->grView({GR_CH_FMT_UNDEFINED, GR_NUM_FMT_UNDEFINED}, sizeof(MaterialConstants));
}

void SimpleMaterial::initGPUMemory()
{
	mConstantsBuffer = GPUMemoryManager::getInstance().allocateBufferFromGPUPool(sizeof(MaterialConstants));

	//GPUMemorySPtr constantsMemory = GPUMemoryManager::getInstance().allocateGPUMemory(sizeof(MaterialConstants));
	//mConstantsBuffer = std::make_shared<Buffer>(constantsMemory, sizeof(MaterialConstants));
}

void SimpleMaterial::updateGPUMemory(CommandBufferSPtr &cmdBuff, const RenderPhaseParamsSPtr &renderParams, const RenderContext &context)
{
	MutexGuard locker(mGuard);

	if (!mDirty)
		return;

	mConstantsBuffer->update(cmdBuff, sizeof(MaterialConstants), &mMaterialConstants);
	mConstantsBuffer->setState(cmdBuff, GR_MEMORY_STATE_GRAPHICS_SHADER_READ_ONLY);

	mDirty = false;
}

void SimpleMaterial::bindData(MemoryRefSet &refs, const DescriptorSetSPtr &descriptorSet, GR_UINT descriptorOffset,
	const RenderPhaseParamsSPtr &renderParams, const RenderContext &context)
{
	MutexGuard locker(mGuard);

	GR_DESCRIPTOR_SET grDescriptorSet = descriptorSet->grDescriptorSet();

	GR_IMAGE_VIEW_ATTACH_INFO attachImage = {};
	attachImage.state = GR_IMAGE_STATE_GRAPHICS_SHADER_READ_ONLY;
	attachImage.view = mAlbedoTexture->grView();

	grAttachMemoryViewDescriptors(grDescriptorSet, 0 + descriptorOffset, 1, &renderParams->scene->constantsView(refs));
	grAttachMemoryViewDescriptors(grDescriptorSet, 1 + descriptorOffset, 1, &renderParams->scene->lightsView(refs));
	grAttachMemoryViewDescriptors(grDescriptorSet, 2 + descriptorOffset, 1, &renderParams->viewport->constantsMemory(refs));
	grAttachMemoryViewDescriptors(grDescriptorSet, 3 + descriptorOffset, 1, &context.renderer->objectConstantsView(refs));
	grAttachMemoryViewDescriptors(grDescriptorSet, 4 + descriptorOffset, 1, &context.renderer->meshConstantsView(refs));
	grAttachMemoryViewDescriptors(grDescriptorSet, 5 + descriptorOffset, 1, &materialConstantsView(refs));
	grAttachSamplerDescriptors(grDescriptorSet, 6 + descriptorOffset, 1, &mSampler);
	grAttachImageViewDescriptors(grDescriptorSet, 7 + descriptorOffset, 1, &attachImage);

	refs.insert(mAlbedoTexture->gpuMemory()->grReference(GR_MEMORY_REF_READ_ONLY));
}

void SimpleMaterial::createPipeline()
{
	RenderSystemResourceManager &resMgr = RenderSystemResourceManager::getInstance();

	mPipeline = resMgr.getPipeline("simplePipeline", SimpleMaterial::createPipelineInfo);
}

void SimpleMaterial::createSampler()
{
	RenderSystem &renderSystem = RenderSystem::getInstance();

	GR_SAMPLER_CREATE_INFO info = {};
	info.filter = GR_TEX_FILTER_MAG_POINT_MIN_POINT_MIP_POINT;
	info.addressU = GR_TEX_ADDRESS_WRAP;
	info.addressV = GR_TEX_ADDRESS_WRAP;
	info.addressW = GR_TEX_ADDRESS_WRAP;
	info.borderColor = GR_BORDER_COLOR_WHITE;
	info.maxLod = 0;
	info.minLod = 0;
	info.mipLodBias = 0.1f;
	info.compareFunc = GR_COMPARE_NEVER;
	info.maxAnisotropy = 1;

	grCreateSampler(renderSystem.device(), &info, &mSampler);
}

const GR_GRAPHICS_PIPELINE_CREATE_INFO& SimpleMaterial::createPipelineInfo()
{
	RenderSystemResourceManager &resourceManager = RenderSystemResourceManager::getInstance();

	GR_SHADER vs = resourceManager.getShader("./shaders/simple/vs.bin");
	GR_SHADER ps = resourceManager.getShader("./shaders/simple/ps.bin");

	// -- VS --
	DESCRIPTOR_SLOT(vsDescriptorSlots, 0, GR_SLOT_SHADER_RESOURCE, 0)
	DESCRIPTOR_SLOT(vsDescriptorSlots, 1, GR_SLOT_SHADER_RESOURCE, 1)
	DESCRIPTOR_SLOT(vsDescriptorSlots, 2, GR_SLOT_SHADER_RESOURCE, 2)
	DESCRIPTOR_SLOT(vsDescriptorSlots, 3, GR_SLOT_SHADER_RESOURCE, 3)
	DESCRIPTOR_SLOT(vsDescriptorSlots, 4, GR_SLOT_SHADER_RESOURCE, 4)

	DESCRIPTOR_SLOT(vsDescriptorSlots, 5, GR_SLOT_UNUSED, 0)
	DESCRIPTOR_SLOT(vsDescriptorSlots, 6, GR_SLOT_UNUSED, 0)
	DESCRIPTOR_SLOT(vsDescriptorSlots, 7, GR_SLOT_UNUSED, 0)

	// -- PS --
	DESCRIPTOR_SLOT(psDescriptorSlots, 0, GR_SLOT_SHADER_RESOURCE, 0)
	DESCRIPTOR_SLOT(psDescriptorSlots, 1, GR_SLOT_SHADER_RESOURCE, 1)
	DESCRIPTOR_SLOT(psDescriptorSlots, 2, GR_SLOT_UNUSED, 0)
	DESCRIPTOR_SLOT(psDescriptorSlots, 3, GR_SLOT_UNUSED, 0)
	DESCRIPTOR_SLOT(psDescriptorSlots, 4, GR_SLOT_UNUSED, 0)

	DESCRIPTOR_SLOT(psDescriptorSlots, 5, GR_SLOT_SHADER_RESOURCE, 2)
	DESCRIPTOR_SLOT(psDescriptorSlots, 6, GR_SLOT_SHADER_SAMPLER, 1)
	DESCRIPTOR_SLOT(psDescriptorSlots, 7, GR_SLOT_SHADER_RESOURCE, 3)

	pipelineCreateInfo.vs.shader = vs;
	pipelineCreateInfo.vs.dynamicMemoryViewMapping.slotObjectType = GR_SLOT_UNUSED;
	pipelineCreateInfo.vs.descriptorSetMapping[0].descriptorCount = DESCRIPTOR_SIZE;
	pipelineCreateInfo.vs.descriptorSetMapping[0].pDescriptorInfo = vsDescriptorSlots;

	pipelineCreateInfo.ps.shader = ps;
	pipelineCreateInfo.ps.dynamicMemoryViewMapping.slotObjectType = GR_SLOT_UNUSED;
	pipelineCreateInfo.ps.descriptorSetMapping[0].descriptorCount = DESCRIPTOR_SIZE;
	pipelineCreateInfo.ps.descriptorSetMapping[0].pDescriptorInfo = psDescriptorSlots;

	pipelineCreateInfo.iaState.topology = GR_TOPOLOGY_TRIANGLE_LIST;
	pipelineCreateInfo.iaState.disableVertexReuse = GR_FALSE;

	pipelineCreateInfo.rsState.depthClipEnable = GR_TRUE;

	pipelineCreateInfo.cbState.logicOp = GR_LOGIC_OP_COPY;
	pipelineCreateInfo.cbState.target[0].blendEnable = GR_FALSE;
	pipelineCreateInfo.cbState.target[0].channelWriteMask = 0xF; // RGBA bits
	pipelineCreateInfo.cbState.target[0].format.channelFormat = GR_CH_FMT_R8G8B8A8;
	pipelineCreateInfo.cbState.target[0].format.numericFormat = GR_NUM_FMT_UNORM;

	pipelineCreateInfo.dbState.format.channelFormat = GR_CH_FMT_R32G8;
	pipelineCreateInfo.dbState.format.numericFormat = GR_NUM_FMT_DS;

	return pipelineCreateInfo;
}

}


}