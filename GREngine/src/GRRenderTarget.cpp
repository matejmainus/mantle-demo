#include "GRRenderTarget.h"
#include "GRRenderSystem.h"
#include "GRRenderSystemResourceManager.h"
#include "GRGPUMemory.h"
#include "GRTexture.h"

namespace GREngine
{

RenderTarget::RenderTarget(const TextureSPtr& texture, Type type) :
	mTexture(texture),
	mColorTargetView(GR_NULL_HANDLE),
	mDepthStencilTargetView(GR_NULL_HANDLE)
{
	mRenderSystem = RenderSystem::getInstancePtr();

	if(type == RenderTarget::Type::COLOR)
	{ 
		GR_COLOR_TARGET_VIEW_CREATE_INFO colorTargetViewCreateInfo = {};
		colorTargetViewCreateInfo.image = mTexture->grImage();
		colorTargetViewCreateInfo.arraySize = 1;
		colorTargetViewCreateInfo.baseArraySlice = 0;
		colorTargetViewCreateInfo.mipLevel = 0;
		colorTargetViewCreateInfo.format.channelFormat = GR_CH_FMT_R8G8B8A8;
		colorTargetViewCreateInfo.format.numericFormat = GR_NUM_FMT_UNORM;

		grCreateColorTargetView(mRenderSystem->device(), &colorTargetViewCreateInfo, &mColorTargetView);
	}
	else
	{
		GR_DEPTH_STENCIL_VIEW_CREATE_INFO depthTargetCreateInfo = {};
		depthTargetCreateInfo.image = mTexture->grImage();
		depthTargetCreateInfo.arraySize = 1;
		depthTargetCreateInfo.baseArraySlice = 0;
		depthTargetCreateInfo.mipLevel = 0;
		depthTargetCreateInfo.flags = 0;

		grCreateDepthStencilView(mRenderSystem->device(), &depthTargetCreateInfo, &mDepthStencilTargetView);
	}
}

RenderTarget::~RenderTarget()
{
	if(mColorTargetView)
		grDestroyObject(mColorTargetView);

	if(mDepthStencilTargetView)
		grDestroyObject(mDepthStencilTargetView);

	mColorTargetView = GR_NULL_HANDLE;
	mDepthStencilTargetView = GR_NULL_HANDLE;
}

const TextureSPtr& RenderTarget::texture() const
{
	return mTexture;
}

void RenderTarget::clear()
{
	float color[] = { 0, 0, 0, 1};
	mTexture->clear(color);
}

void RenderTarget::present()
{}

void RenderTarget::prepare()
{
	if (mColorTargetView)
		mTexture->setState(GR_IMAGE_STATE_TARGET_RENDER_ACCESS_OPTIMAL);
	else if (mDepthStencilTargetView)
		mTexture->setState(GR_IMAGE_STATE_TARGET_SHADER_ACCESS_OPTIMAL);
}

GR_COLOR_TARGET_VIEW RenderTarget::colorTargetView() const
{
	return mColorTargetView;
}

GR_DEPTH_STENCIL_VIEW RenderTarget::depthStencilTargetView() const
{
	return mDepthStencilTargetView;
}

}