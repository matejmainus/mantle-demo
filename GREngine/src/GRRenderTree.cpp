#include "GRRenderTree.h"
#include "GRRenderer.h"
#include "GRMaterial.h"
#include "GRPipeline.h"

namespace GREngine
{
	RenderTree::RenderTree()
	{
		mStart = mNodes.end();
	}

	RenderTree::~RenderTree()
	{
	}

	void RenderTree::add(const RenderContext context)
	{
		if (!context.renderer->canDraw())
			return;

		RenderNodeSPtr node = nullptr;

		if (mStart == mNodes.end())
		{
			node = std::make_shared<RenderNode>();
			node->pipeline = context.renderer->material()->pipeline();

			mNodes.push_back(node);
			mStart = --mNodes.end();
		}
		else
		{
			for (RenderNodeIterator it = mStart; it != mNodes.end(); ++it)
			{
				if (it->get()->pipeline->grPipeline() == context.renderer->material()->pipeline()->grPipeline() 
					&& it->get()->childs.size() < RenderNode::maxBatchCount())
				{
					node = *it;
					break;
				}
			}

			if (node == nullptr)
			{
				node = std::make_shared<RenderNode>();
				node->pipeline = context.renderer->material()->pipeline();

				mNodes.push_back(node);
			}
		}

		node->childs.push_back(context);

		if (mStart->get()->childs.size() == RenderNode::maxBatchCount())
			mStart++;
	}

	RenderNodeIterator RenderTree::begin()
	{
		return mNodes.begin();
	}

	RenderNodeIterator RenderTree::end()
	{
		return mNodes.end();
	}

	RenderNodeIterator_const RenderTree::begin() const
	{
		return mNodes.cbegin();
	}

	RenderNodeIterator_const RenderTree::end() const
	{
		return mNodes.cend();
	}

	RenderNodeIterator_const RenderTree::cbegin() const
	{
		return mNodes.cbegin();
	}

	RenderNodeIterator_const RenderTree::cend() const
	{
		return mNodes.cend();
	}
}