#include <assimp\mesh.h>

#include "GRMesh.h"

#include "GRMeshManager.h"

#include "GRGPUMemoryManager.h"
#include "GRBuffer.h"
#include "GRRenderSystemResourceManager.h"
#include "GRGPUMemory.h"
#include "GRDescriptorSet.h"
#include "GRCommandBuffer.h"
#include "GRMemoryRefSet.h"

namespace GREngine
{
	MeshSPtr Mesh::create(const std::string file)
	{
		MeshManager &mgr = MeshManager::getInstance();

		return mgr.loadMesh(file);
	}

	Mesh::Mesh(aiMesh *mesh) :
		mMesh(mesh)
	{
		initGpuMemory();
		copyToGpu();
	}

	Mesh::~Mesh()
	{
	}

	GR_MEMORY_VIEW_ATTACH_INFO Mesh::meshView(MemoryRefSet &refs) const
	{
		refs.insert(mMeshBuffer->grReference(GR_MEMORY_REF_READ_ONLY));

		return mMeshBuffer->grView({GR_CH_FMT_UNDEFINED, GR_NUM_FMT_UNDEFINED}, sizeof(RenderMeshVertex));
	}

	GR_MEMORY_VIEW_ATTACH_INFO Mesh::indexiesView(MemoryRefSet &refs) const
	{
		refs.insert(mIndexiesBuffer->grReference(GR_MEMORY_REF_READ_ONLY));

		return mIndexiesBuffer->grView({}, 0);
	}

	GR_UINT Mesh::indexiesCount() const
	{
		return mMesh->mNumFaces * mMesh->mFaces->mNumIndices;
	}

	GR_UINT Mesh::verticesCount() const
	{
		return mMesh->mNumVertices;
	}

	void Mesh::initGpuMemory()
	{
		size_t meshSize = sizeof(RenderMeshVertex) * verticesCount();
		size_t indexiesSize = sizeof(unsigned short) *  indexiesCount();

		//+4 due to alignment for indexies
		GPUMemorySPtr meshMem = GPUMemoryManager::getInstance().allocateGPUMemory(meshSize + indexiesSize, 0, GR_MEMORY_PRIORITY_VERY_HIGH);
		mMeshBuffer = std::make_shared<Buffer>(meshMem, meshSize);
		mIndexiesBuffer = std::make_shared<Buffer>(meshMem, indexiesSize);
	}

	void Mesh::copyToGpu()
	{
		RenderMeshVertex *vertices = new RenderMeshVertex[verticesCount()];
		unsigned short *indexies = new unsigned short[indexiesCount()];

		const size_t faceIndexCount = mMesh->mFaces->mNumIndices;
		const size_t meshSize = sizeof(RenderMeshVertex) * verticesCount();
		const size_t indexiesSize = sizeof(unsigned short) *  indexiesCount();

		RenderMeshVertex vertex;
		aiVector3D aiVec;

		float vec[4] = {0.0f, 0.0f, 0.0f, 1.0f};

		for (size_t i = 0; i < verticesCount() ; ++i)
		{
			memcpy(vec, &mMesh->mVertices[i], 3 * sizeof(float));
			vertex.pos = DirectX::XMFLOAT4(vec);

			if (mMesh->HasNormals())
			{
				memcpy(vec, &mMesh->mNormals[i], 3 * sizeof(float));
				vertex.normal = DirectX::XMFLOAT4(vec);
			}

			if (mMesh->GetNumUVChannels() > 0)
			{
				memcpy(vec, &mMesh->mTextureCoords[0][i], 2 * sizeof(float));
				vertex.uv = DirectX::XMFLOAT2((vec));
			}

			vertices[i] = vertex;
		}

		for (size_t i = 0; i < mMesh->mNumFaces; ++i)
		{
			for (size_t j = 0; j < faceIndexCount; ++j)
				indexies[j + i * faceIndexCount] = (unsigned short) mMesh->mFaces[i].mIndices[j];
		}

		GPUMemorySPtr gpuMemory = mMeshBuffer->gpuMemory();
		GPUMemorySPtr stageBuffer = GPUMemoryManager::getInstance().allocateGPUMemory(gpuMemory->grInfo().size, GR_MEMORY_HEAP_CPU_VISIBLE);

		char *bufferPtr = (char*) stageBuffer->map();

		memcpy(bufferPtr + mMeshBuffer->offset(), vertices, meshSize);
		memcpy(bufferPtr + mIndexiesBuffer->offset(), indexies, indexiesSize);

		stageBuffer->unmap();

		GR_MEMORY_COPY copy = {};
		copy.srcOffset = 0;
		copy.destOffset = 0;
		copy.copySize = meshSize + indexiesSize;

		CommandBufferSPtr cmdBuff = CommandBuffer::getFromPool(GR_CMD_BUFFER_OPTIMIZE_ONE_TIME_SUBMIT);

		mMeshBuffer->setState(cmdBuff, GR_MEMORY_STATE_DATA_TRANSFER);
		mIndexiesBuffer->setState(cmdBuff, GR_MEMORY_STATE_DATA_TRANSFER);

		grCmdCopyMemory(cmdBuff->grCmdBuffer(), stageBuffer->grMemory(), gpuMemory->grMemory(), 1, &copy);

		mMeshBuffer->setState(cmdBuff, GR_MEMORY_STATE_GRAPHICS_SHADER_READ_ONLY);
		mIndexiesBuffer->setState(cmdBuff, GR_MEMORY_STATE_INDEX_DATA);

		MemoryRefSet refsSet;

		refsSet.insert(stageBuffer->grReference(GR_MEMORY_REF_READ_ONLY));
		refsSet.insert(gpuMemory->grReference());
		cmdBuff->queueBlocking(refsSet);

		CommandBuffer::returnToPool(cmdBuff);

		delete vertices;
		delete indexies;
	}
}